<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Account;

class AccountsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 10; $i++) { 
            $account = new Account();
            $account->setName('product '.$i);
            $account->setTotal(mt_rand(100, 1000));
            
            $manager->persist($account);
        }
        $manager->flush();
    }
}
