<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;

class OperationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i=0; $i < 10 ; $i++) { 
            $operation = new Operation();
            $operation->setName('operation'. $i);
            $operation->setCategory(null);
            $operation->setPrice(mt_rand(10, 100));
            $operation->setType(true);
            $operation->setDate(new \DateTime('06/04/2014'));
    
            $manager->persist($operation);
        }
        
    

        $manager->flush();
    }
    
}
