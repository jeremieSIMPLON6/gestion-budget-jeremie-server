<?php

namespace App\Controller;

use App\Entity\Operation;
use App\Form\OperationType;
use App\Repository\OperationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Category;

/**
 * @Route("/operation")
 */
class OperationController extends Controller
{

    private $serializer;

    public function __construct()
    {

    }


    /**
     * @Route("/", name="operation_index", methods="GET")
     */
    public function index(OperationRepository $operationRepository)
    {

        $this->serializer = $this->get('jms_serializer');

        $list = $operationRepository->findAll();


        $response = new Response($this->serializer->serialize($list, "json"));
        return $response;



        // return $this->render('operation/index.html.twig', ['operations' => $operationRepository->findAll()]);
    }



    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $req)
    {

        $this->serializer = $this->get('jms_serializer');

        $operation = $this->serializer->deserialize($req->getContent(), Operation::class, 'json');
        
        // $data = json_decode($req->getContent(), true);

        $category = $this->getDoctrine()->getRepository(Category::class)->find($operation->getCategory()->getId());

        // $operation = new Operation;
        // $operation->setName($data["name"]);
        // $operation->setType($data["type"]);
        // $operation->setPrice($data["price"]);

        // $date = new \DateTime();
        // $date->createFromFormat('d/m/Y', $data["date"]);
        // $operation->setDate($date);

        if($category != null) {
            $operation->setCategory($category);
        }
        $manager = $this->getDoctrine()->getManager();
        $manager->merge($operation->getCategory());
        $manager->persist($operation);
        $manager->flush();

        $json = $this->serializer->serialize($operation, "json");

        return JsonResponse::fromJsonString($json);

    }

    /**
     * @Route("/{id}", name="operation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Operation $operation) : Response
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($operation);
        $em->flush();

        $json = $this->serializer->serialize($operation, "json");
        return JsonResponse::fromJsonString($json);
    }
}
