<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/category")
 */
class CategoryController extends Controller
{

    private $serializer;

    public function __construct()
    {
        // $encoder = new JsonEncoder();
        // $normalizer = new ObjectNormalizer();
        // $normalizer->setCircularReferenceLimit(1);
        // $normalizer->setCircularReferenceHandler(function ($object) {
        //     return $object->getId();
        // });
        // $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/", name="category_index", methods="GET")
     */
    public function index(CategoryRepository $categoryRepository)
    {

        $this->serializer = $this->get('jms_serializer');

        $list = $categoryRepository->findAll();

        // $data = $this->serializer->normalize($list, null, ['categories' => ['id', 'name', 'image']]);

        $response = new Response($this->serializer->serialize($list, "json"));
        return $response;


    }






    /**
     * @Route("/", name="category_new", methods="POST")
     */
    public function add(Request $request) : Response
    {
        // $category = new Category();
        // $form = $this->createForm(CategoryType::class, $category);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $em = $this->getDoctrine()->getManager();
        //     $em->persist($category);
        //     $em->flush();

        //     return $this->redirectToRoute('category_index');
        // }

        // return $this->render('category/new.html.twig', [
        //     'category' => $category,
        //     'form' => $form->createView(),
        // ]);

                                      // NEW CODE //

        $this->serializer = $this->get('jms_serializer');

        $category = $this->serializer->deserialize($request->getContent(), Category::class, 'json');


        // $category = $this->getDoctrine()->getRepository(Category::class)->find($category->getCategory());

        // $operation = new Operation;
        // $operation->setName($data["name"]);
        // $operation->setType($data["type"]);
        // $operation->setPrice($data["price"]);

        // $date = new \DateTime();
        // $date->createFromFormat('d/m/Y', $data["date"]);
        // $operation->setDate($date);

        // if ($category != null) {
        //     $category->setCategory($category);
        // }
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($category);
        $manager->flush();

        $json = $this->serializer->serialize($category, "json");

        return JsonResponse::fromJsonString($json);


    }

    /**
     * @Route("/{id}", name="category_show", methods="GET")
     */
    public function show(Category $category) : Response
    {
        return $this->render('category/show.html.twig', ['category' => $category]);
    }

    /**
     * @Route("/{id}/edit", name="category_edit", methods="GET|POST")
     */
    public function edit(Request $request, Category $category) : Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_edit', ['id' => $category->getId()]);
        }

        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_delete", methods="DELETE")
     */
    public function delete(Request $request, Category $category) : Response
    {
        if ($this->isCsrfTokenValid('delete' . $category->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('category_index');
    }
}
